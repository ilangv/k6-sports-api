import { describe } from "https://jslib.k6.io/expect/0.0.5/index.js";
import { Httpx } from "https://jslib.k6.io/httpx/0.0.4/index.js";

// Init session with auth token
let session = new Httpx({
  baseURL: "https://api.football-data.org",
  headers: {
    "X-Auth-Token": "9ac9918ad94243428c158b3ceb8c1e2a",
  },
});

// Test Parameters
const SEASON = "2020";
const LEAGUE = "PL";

// Add parameterized endpoints
const endpoints = {
  scorers: `/v2/competitions/${LEAGUE}/scorers?season=${SEASON}`,
  standings: `/v2/competitions/${LEAGUE}/standings?season=${SEASON}`,
};

export default function testSuite() {
  describe("Fetch top 10 players", (t) => {
    // Get top 10 scoring players
    let response = session.get(endpoints.scorers);
    t.expect(response.status)
      .as("response status")
      .toEqual(200)
      .and(response)
      .toHaveValidJson();
    const { scorers } = response.json();
    const topTenScorers = scorers.slice(0, 10);

    // Print top 10 scoring players
    console.log("--------- TOP 10 PLAYERS ---------");
    topTenScorers.map((scorer) =>
      console.log(`${scorer.player.name} - ${scorer.numberOfGoals}`)
    );
    console.log("----------------------------------\n");

    // Did not find a team stats API so generating team wise status from player API response
    const teamReducer = (teams, scorer) => {
      const { team } = scorer;
      const goalcount = teams[team.id] ? teams[team.id].goals : 0;
      const team_score = {
        [team.id]: {
          goals: scorer.numberOfGoals + goalcount,
          name: team.name,
        },
      };
      return Object.assign({}, teams, team_score);
    };
    const teams = scorers.reduce(teamReducer, {});

    // Convert reduced object to array for easier iteration
    const teams_arr = Object.entries(teams).map(([key, value]) =>
      Object.assign({}, { id: key }, value)
    );
    const sorted_teams = teams_arr.sort((a, b) => b.goals - a.goals);
    const topTenTeams = sorted_teams.slice(0, 10);

    // Get team standings for season
    const standings_response = session.get(endpoints.standings);
    t.expect(standings_response.status)
      .as("response status")
      .toEqual(200)
      .and(standings_response)
      .toHaveValidJson();
    const { standings } = standings_response.json();

    // Print top 10 scoring teams ordered by season rank
    console.log("\n--------- TOP 10 TEAMS ---------");
    standings[0].table.forEach((standings_team) => {
      const match = topTenTeams.find(
        (team) => team.id.toString() == standings_team.team.id
      );
      if (match) {
        console.log(
          `Rank: ${standings_team.position} Name: ${match.name} Goals: ${match.goals}`
        );
      }
    });
    console.log("----------------------------------\n");
  });
}
