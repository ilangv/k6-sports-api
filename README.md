# k6-sports-api

Functional test sports API using k6.io

## Requirement

To find top 10 players and top 10 teams for a given leage and season
## APIs used

Following APIs were used from football-data.org

Player info:
https://api.football-data.org/v2/competitions/PL/scorers?season=2020

Standings info:
https://api.football-data.org/v2/competitions/PL/standings?season=2020


## Running the test


On MacOS ensure k6 is installed :
```
brew install k6
```

Run tests with command:

```
k6 run index.js
```
